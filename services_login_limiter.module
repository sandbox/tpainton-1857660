<?php
/**
 * @file
 * Limits login to users without granted role permissions to
 * logging in via Services. Replaces user.resource.inc in Services
 * module to make a check for remote login, if not present,
 * immediately logs the user out. This module should be used in
 * conjuction with a session limter to prevent people from logging
 * on peripherally, then using the the session cookie to login locally.
 *
 * USAGE:  In order for this module to function properly, you
 * should enable the two actions included with the remote_user
 * resource; 'login' and 'register' (if you want to enable users to
 * create an account).
 *
 * To login via your new service, go to
 * admin/config/system/services_login_limiter and enter your chosen
 * key.  This key is required to complete the login or registration.
 *
 * To register user, POST to your register endpoint and include the
 * data as below, this is in JSON but you can use any other Services
 * format.
 *
 * {
 *   "account":
 *     {'
 *       "name":"joe",
 *       "pass":"mypassword",
 *       "mail":"joe@home.com"
 *     },
 *   "key":"Key entered in configuration"
 * }
 *
 * RESPONSE : {"uid":"734","uri":"http://www.myserver.com/rest/user/734"}
 *
 * Note, just as in Services, the user at this point will be logged in.
 *
 * To login an already created user; POST to your login endpoint,
 * {
 *   "username":"joe",
 *   "password":"mypassword",
 *   "key":"Key entered in configuration"
 * }
 *
 * The response is the same as Service's user.resource, 'login' action.
 *
 *
 * A few Notes:
 *
 * Your should probably disable the Services included user
 * resource action 'login', and CRUD operation 'create'
 *
 * You should obfuscate your key in the client application.
 * Not doing so could result in a user spoofing a remote login
 * using HttpRequester, or other browser based plugin.
 *
 * You should disable simultaneous logins using
 * Session Limit module (or other tool). Failing to do so will
 * allow a user to login remotely, then also locally once the the
 * session is established.
 */


define('LOCAL_LOGIN', 'local login');
define('LOGIN_LIMITER_KEY', 'login_limiter_key');
define('LOGIN_LIMITER_MSG', 'login_limiter_message');


/**
 * Implements hook_help().
 */
function services_login_limiter_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/config/structure/sll':
      $output = t('This page is used to configure site-wide features of the Services Login Limiter module.');
      break;
  }
  return $output;
}

/**
 * Implements hook_menu().
 */
function services_login_limiter_menu() {
  $items = array();
  $items['admin/config/system/services_login_limiter'] = array(
    'title'             => 'Services Login Limiter',
    'description'       => 'Configure options for Services Login Limiter',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('services_login_limiter_settings'),
    'access callback'   => 'user_access',
    'access arguments'  => array('administer site configuration'),
    'type'              => MENU_NORMAL_ITEM,
    'file'              => 'includes/services_login_limiter.admin.inc',

  );

  return $items;
}


/**
 * Implements hook_user_login().
 */
function services_login_limiter_user_login(&$edit, $account) {

  // A user has logged in.  If they don't have the appropriate role
  // we check to see if they are flagged as remote
  // if not, we log them off, and let them know local logins are not allowed.
  global $user;

  if (!user_access(LOCAL_LOGIN)) {
    $wireless = variable_get($account->uid . '_logged', NULL);
    if (!$wireless) {
      watchdog('debug', 'Failed wireless check');
      session_destroy();
      $user = drupal_anonymous_user();
      drupal_set_message(t(variable_get(LOGIN_LIMITER_MSG, 'Local login not allowed.')));
      drupal_goto();
    }
  }

  // Clear the system variable.
  $tag = $account->uid . '_logged';
  if (variable_get($tag, NULL) != NULL) {
    variable_del($tag);
  }
  return;
}

/**
 * Implements hook_user_insert().
 */
function services_login_limiter_user_insert(&$edit, $account, $category) {

  // A user has been created.  We need to check and see if they are remote user.
  // If so, then set the variable to flag they are.
  if (isset($_SESSION[$account->name . $account->mail]['remote']) && $_SESSION[$account->name . $account->mail]['remote'] == 'true') {
    variable_set($account->uid . '_logged', TRUE);
    return;
  }
  else {
    // First make sure the current user isn't someone able to create accounts.
    if (!user_access('administer users')) {
      // Insert uid into the delete table.
      services_login_limiter_queue_delete($account->uid);
    }
  }

}

/**
 * Implements hook_cron().
 */
function services_login_limiter_cron() {
  // Delete 1 from database, just in case it ends up there.
  db_delete('services_login_limiter_deletes')
        ->condition('uid', 1)
        ->execute();
  $result = db_query("SELECT uid FROM {services_login_limiter_deletes}");
  foreach ($result as $record) {
    user_delete($record->uid);
  }
  // Now clear the db.
  db_truncate('services_login_limiter_deletes')->execute();
}

/**
 * Implements hook_permission().
 */
function services_login_limiter_permission() {
  return array(
    LOCAL_LOGIN => array('title' => t('Login locally')),
  );
}

/**
 * Implements hook_services_resources().
 */
function services_login_limiter_services_resources() {
  return array(
    'remote_user' => array(
      'actions' => array(
        'login' => array(
          'callback' => '_services_login_limiter_remote_login',
          'access callback' => 'services_login_limiter_access_menu',
          'args' => array(
            array(
              'name' => 'username',
              'type' => 'string',
              'description' => 'A valid username',
              'source' => array('data' => 'username'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'password',
              'type' => 'string',
              'description' => 'A valid password',
              'source' => array('data' => 'password'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'key',
              'type' => 'string',
              'description' => 'Key to be included in the remote application',
              'source' => array('data' => 'key'),
              'optional' => FALSE,
            ),
          ),
          'help' => t('Provides a services endpoint that allows only remote login. Will not work on newly registered users.  You must also use the included register resource'),
        ),

        'register' => array(
          'help' => 'Creates a user',
          'callback' => '_services_login_limiter_remote_register',
          'access callback' => 'services_login_limiter_access_menu',
          'args' => array(
            array(
              'name' => 'account',
              'type' => 'array',
              'description' => 'The user object',
              'source' => array('data' => 'account'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'key',
              'type' => 'string',
              'description' => 'The validation key.',
              'source' => array('data' => 'key'),
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),
  );
}

/**
 * Returns TRUE for those services endpoints that are always available.
 */
function services_login_limiter_access_menu() {
  return TRUE;
}

/**
 * Registers a user remotely and sets a flag that allows subsequent login.
 */
function _services_login_limiter_remote_register($account, $key) {

  // Check to be sure this login is coming from our outside client.
  if ($key == variable_get(LOGIN_LIMITER_KEY, NULL)) {
    // Flag this anonymous user as remote client.
    $_SESSION[$account['name'] . $account['mail']]['remote'] = 'true';
  }

  // Adds backwards compatability with regression fixed in Services #1083242
  $account = _services_login_limiter_arg_value($account, 'account');

  // Load the required includes for saving profile information
  // With drupal_form_submit().
  module_load_include('inc', 'user', 'user.pages');

  // Register a new user.
  $form_state['values'] = $account;
  $form_state['values']['pass'] = array(
    'pass1' => $account['pass'],
    'pass2' => $account['pass'],
  );
  $form_state['values']['op'] = variable_get('services_user_create_button_resource_create', t('Create new account'));

  // Execute the register form.
  drupal_form_submit('user_register_form', $form_state);
  // Find and store the new user into the form_state.
  if (isset($form_state['values']['uid'])) {
    $form_state['user'] = user_load($form_state['values']['uid']);
  }

  // Error if needed.
  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }
  else {
    $user = array('uid' => $form_state['user']->uid);
    if ($uri = services_resource_uri(array('user', $user['uid']))) {
      $user['uri'] = $uri;
    }
    return $user;
  }
}

/**
 * Remote login callback for login endpoint.
 *
 * Fairly similar to Services user.resource.inc
 * _user_resource_login($username, $password) except this
 * function takes the 'key' provided in the login array and stores
 * it in a system variable with the username as an array key.
 * Later when, hook_user_login() is implemented, we check
 * for the presence of this variable.  If it is null, we log the user out.
 *
 * @param String $username
 *   The user's login
 * @param String $password
 *   The user's password
 * @param String $key
 *   the passphrase passed via services that must
 *   match the defined keyphrase in settings.
 */
function _services_login_limiter_remote_login($username, $password, $key) {

  global $user;

  if ($user->uid) {
    // User is already logged in.
    return services_error(t('Already logged in as @user.', array('@user' => $user->name)), 406);
  }

  $uid = user_authenticate($username, $password);

  if ($uid) {

    // Check to make sure the correct key is passed, and if so,
    // store the ID as TRUE.
    $result = services_login_limiter_handle_key($uid, $key);

    // If the key is valid, proceed with login.
    if ($result) {

      $user = user_load($uid);
      if ($user->uid) {
        user_login_finalize();
        $return = new stdClass();
        $return->sessid = session_id();
        $return->session_name = session_name();
        services_remove_user_data($user);
        $return->user = $user;
        return $return;
      }
    }
    else {
      watchdog('user', 'Invalid key for %username.', array('%username' => $username));
      return services_error(t('Wrong remote key provided'), 401);
    }
  }
  watchdog('user', 'Failed user authentication for %username.', array('%username' => $username));
  return services_error(t('Wrong username or password.'), 401);
}

/**
 * Checks key and flags user logged.
 *
 * Takes the key passed via services and checks to see if it's held
 * in system variables.  If so, then this user has logged in via
 * service resource remote_user/login and returns TRUE.
 *
 * @param String $uid
 *   The user id.
 * @param String $key
 *   The application key.
 */
function services_login_limiter_handle_key($uid, $key) {
  if ($key == variable_get(LOGIN_LIMITER_KEY, NULL)) {
    variable_set($uid . '_logged', TRUE);
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Inserts a uid into the pending deletes table.
 *
 * @param String $uid
 *   The user uid
 */
function services_login_limiter_queue_delete($uid) {
  db_insert('services_login_limiter_deletes')
    ->fields(array(
      'uid' => $uid,
    ))
    ->execute();
}

/**
 * Adds backwards compatability with regression fixed in Services #1083242.
 */
function _services_login_limiter_arg_value($data, $field) {
  if (isset($data[$field]) && count($data) == 1 && is_array($data[$field])) {
    return $data[$field];
  }
  return $data;
}
