<?php 

/**
 * @file
 * Configuration pages for Services Login Limiter module.
 */

/**
 * Implements hook_settings().
 */
function services_login_limiter_settings($form, $form_state) {
  $form['options'] = array(
    '#tree' => FALSE,
    '#type' => 'fieldset',
    '#title' => t('Services Login Limiter Global Settings.'),
    '#weight' => 3,
  );

  $form['options'][LOGIN_LIMITER_MSG] = array(
    '#type' => 'textfield',
    '#title' => t('Local login refusal message.'),
    '#description' => t('Message to display when local login refused.'),
    '#default_value' => variable_get(LOGIN_LIMITER_MSG, 'Local login not allowed.'),
    '#required' => TRUE,
    '#size' => 60,
    '#maxlength' => 64,
  );

  $form['options'][LOGIN_LIMITER_KEY] = array(
    '#type' => 'textfield',
    '#title' => t('Key Phrase'),
    '#description' => t('What is the string that must be passed from the client to complete login?'),
    '#default_value' => variable_get(LOGIN_LIMITER_KEY, ''),
    '#required' => TRUE,
    '#size' => 60,
    '#maxlength' => 64,
  );

  return system_settings_form($form);
}
