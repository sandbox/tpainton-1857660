Services Login Limiter module limits logins on a drupal installation 
to a custom service resource and prohibits local logins to all users 
without the appropriate role with permission 'login locally'.  

The module provides a Services module resource 'remote_user' with 
action 'login' which can be selected  from the resource list of the 
Services module.  A key must be passed to the resource along with the 
usual parameters 'username', and 'password'. This key must be set 
in the Services Login Limiter configuration screen.  

This key is then passed as 'proof' that the user is accessing 
the the drupal installation from the designated remote application 
be it, an Android, iOS, or other remote platform.  For this reason, 
the key should be obfuscated in the client application to prevent 
its use with local applications such as  a browser plug in like Poster, 
or HttpRequester for Firefox.  

http://digital-identity.dk/2010/12/protecting-ip-in-android-applications/ 
is a good example of such practice.

To prevent subsequent access locally after remote login, it's 
recommended to install a session limiter such as Session Limit, 
http://drupal.org/project/session_limit.

It's also recommended that the user.resource action 'login' be disabled 
to prevent users from bypassing this module's custom action.  

Module Notes:

 Limits login to users without granted role permissions to 
 logging in via Services.  Replaces user.resource.inc in Services 
 module to make a check for remote login, if not present - immediately 
 logs the user out. This module should be used in conjuction with a 
 session limter to prevent people from logging on peripherally, then 
 using thethe session cookie to login locally. 
  
USAGE:  In order for this module to function properly, you should 
enable the two actions  included with the remote_user resource; 
 
'login' and 'register' (if you want to enable users to create an account).
To login via your new service, go to 
admin/config/system/services_login_limiter and enter your chosen key.  
This key is required to complete the login or registration. To register 
user, POST to your register endpoint and include the data as below, 
this is in JSON but you can use any other Services format.
{
 "account":
   {
     "name":"joe",
     "pass":"mypassword",
     "mail":"joe@home.com"
   },
 "key":"Key entered in configuration"
}
 
RESPONSE : {"uid":"734","uri":"http://www.myserver.com/rest/user/734"}
Note, just as in Services, the user at this point will be logged in.
 
To login an already created user; POST to your login endpoint,
{
  "username":"joe",
  "password":"mypassword",
  "key":"Key entered in configuration"
}

The response is the same as Service's user.resource, 'login' action.  

A few Notes: 
Your should  probably disable the Services included user
resource action 'login', and CRUD operation 'create' 
You should obfuscate your key in the client application.  
Not doing so could result in a user spoofing a remote login 
using HttpRequester, or other browser based plugin.  

You should disable simultaneous logins using 
Session Limit module (or other tool) Failing to do so will allow 
a user to login remotely, then also locally once the the session is 
established.
